<?php

class MySiteTest extends WebTestCase {
    
    function testHomePageHasContactDetailsLink() {
        $this->get('http://www.ramon-barros.com/');
        $this->assertTitle('Home - Ramon Barros / Blog');
        $this->clickLink('contato');
        $this->assertTitle('Home - Ramon Barros / Blog');
        $this->assertText('Ramon Barros');
    }
}