<?php


class LoadFiles extends TestSuite {
	
	private $class = array();
	private $load_files = array();
	private $class_dir;
	private $tests_dir;
	private $file_prefix;
	private $run_test;
	private $file_class = array();
	private $last_class;

	public function __construct($class=null)
	{
		$this->run_test = &$_REQUEST;
		$this->_label = 'Test Suite';
		$this->file_prefix = '_test';
		$this->class = $class;
		//$this->path = realpath(dirname(__FILE__)).'/';
		//$this->path = '/var/www/Dropbox/Public/minhas_classes/';
		//$this->class_dir = $this->path.'classes/';
		//$this->tests_dir = $this->path.'test/';
		$this->class_dir = CLASS_DIR;
		$this->tests_dir = TESTS_DIR;
		$this->check_class();
		$this->selected_test();
	}

	private function selected_test(){
		$this->_label = 'Test Suite';
		if (isset($this->run_test) && count($this->run_test)>0)
		{
			if(isset($this->run_test['all'])){
				$this->load_files();
			}elseif(isset($this->run_test['test'])){
				$this->load_files();
			}else{
				foreach ($this->load_files as $key=>$value) {
					if(isset($this->run_test[strtolower($key)])){
						$load_files[$key] = $this->load_files[$key];
					}
				}
				$this->load_files = $load_files;
				$this->load_files();
			}
		}else{
			$this->load_files();
		}
	}

	/**
	 * Verifica se foi passado um array, string ou null.
	 * @return [type] [description]
	 */
	private function check_class(){
		if(count($this->class)>0)
		{
			if(is_array($this->class)){
				$this->check_files('array');
			}else{
				$this->check_files('string');
			}	
		}else{
			$this->check_files(null);
		}		
	}

	/**
	 * Verifica quais tipos de arquivos vai ser carregado
	 * @param  [type] $type=null [description]
	 * @return [type]            [description]
	 */
	private function check_files($type=null){
		if($type=='array'){
			$this->check_array();
		}elseif($type=='string'){
			$this->check_string();
		}elseif(is_null($type)){
			$this->check_is_null();
		}
	}

	/**
	 * array(
	 * 	Class1,
	 * 	Class2,
	 * 	Class3=>array(test1,test2,test3)
	 * 	class4,
	 * )
	 */
	private function check_array(){
		foreach ($this->class as $key=>$value) {
			if(is_array($value)){
				$this->load_files[$key] = $value;
			}else{
				if(preg_match('/(.+).php/', $value,$math)){
					$this->load_files[$math[1]] = $value;
				}else{
					$this->load_files[$value] = 'ALL';
				}		
			}
		}
		//$this->load_files();
	}

	private function check_string(){
		$this->load_files[$this->class] = 'ALL';
		//$this->load_files();
	}

	private function check_is_null(){
		$this->load_files['ALL'] = 'ALL';
		//$this->load_files();
	}

	private function load_files(){
		foreach ($this->load_files as $key => $value) {
			if(is_dir($this->class_dir.$key) && file_exists($this->class_dir.$key.'/'.$key.'.php')){
				$this->last_class = $key;
				$this->load_class($this->class_dir.$key,$key,'.php');
				if(is_array($value)){
					foreach ($value as $file) {
						$dir = $this->tests_dir.$key.'/';
						$filename = $file.$this->file_prefix;
						if(file_exists($dir.$filename.'.php')){
							$this->load_test(rtrim($dir,'/'),$filename,'.php','/('.strtolower($key).')([\w\.]+)(test).php/');
						}else{
							$this->load_test($this->tests_dir.$key,'*','.php','/('.strtolower($key).')([\w\.]+)(test).php/');
						}
					}
				}else{
					$this->load_test($this->tests_dir.$key,'*','.php','/('.strtolower($key).')([\w\.]+)(test).php/');
				}
			}elseif($key=='ALL'){
				$this->load_class($this->class_dir,'*','.php');
				$this->load_test($this->tests_dir,'*','.php','/([\w\.]+)(test).php/');
			}else{
				$this->load_test(rtrim($this->tests_dir,'/'),'*','.php','/([\w\.]+)(test).php/');
			}
		}
	}

	private function load_class( $path = NULL, $file='*',$extension = '*' ,$regexp=NULL) {
		$this->load($path,$file,$extension,$regexp,'REQUIRE_ONCE');
	}

	private function load_test($path = NULL, $file='*',$extension = '*' ,$regexp=NULL){
		return $this->load($path,$file,$extension,$regexp,'ADDFILE');
	}

	private	function load( $path = NULL, $filename='*', $extension = '*', $regexp,$type='ADDFILE') {
		$path = $path == FALSE ? "" : rtrim($path.'/');
		$extension = $extension == FALSE ?  "*" : $extension;
	    $filename = $filename == FALSE ? "*" : $filename.$extension;
	    $glob = glob($path.$filename);
	    foreach( $glob AS $arq ) 
	    {
	        if( is_dir( $arq ) ) 
	        {
	           $this->load($arq,$filename,$extension,$regexp,$type);
	        } else {

	        	if(strlen($regexp)>0)
	        	{
		        	if(preg_match($regexp,$arq))
		        	{
		            	$this->load_type($arq,$type);
		        	}
		        }else{
					$this->load_type($arq,$type);
		        }
	        }
	    }
	}

	private	function load_type($file,$type){
		switch ($type) 
		{
			case 'INCLUDE':
				//echo 'INCLUDE:'.$file.'<br/>';
				include($file);
				break;
			case 'INCLUDE_ONCE':
				//echo 'INCLUDE_ONCE:'.$file.'<br/>';
				include_once($file);
				break;
			case 'REQUIRE':
				//echo 'REQUIRE:'.$file.'<br/>';
				require($file);
				break;
			case 'REQUIRE_ONCE':
				//echo 'REQUIRE_ONCE:'.$file.'<br/>';
				require_once($file);
				break;
			case 'ADDFILE':
			default:
				//echo 'ADDFILE:'.$file.'<br/>';
				$this->file_class[$this->last_class][] = $file;
				$this->addFile($file);
			break;
		}
	}

	public function run_report($reporter){
		$this->_label = 'Test Suite';
		$this->run($reporter);
	}

	public function get_files(){
		return $this->file_class;
	}
	public function get_class(){
		return $this->load_files;
	}
}