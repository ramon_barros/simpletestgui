<?php

require_once(dirname(__FILE__) . '/reporter.php');

class ShowPasses extends HtmlReporter
{
	var $trId=0;

	function paintLine($message, $result)
	{
	  $breadcrumb = $this->getTestList();
	  array_shift($breadcrumb);
	  $line =
	    "<tr class=\"$result list_".($this->trId%2)."\">".
	      "<td valign=\"top\" class=\"date\" nowrap=\"nowrap\" width=\"5%\">".date('Y-m-d H:i:s')."</td>".
	      "<td valign=\"top\" class=\"method\" nowrap=\"nowrap\" width=\"5%\">".implode("-&gt;", $breadcrumb)."</td>".
	      "<td valign=\"top\" class=\"message\">".$message."</td>".
	      "<td valign=\"top\" class=\"result\" nowrap=\"nowrap\" width=\"5%\">[".strtoupper($result)."]</td>".
	    "</tr>";

	  echo $line;
	 
	  flush();
	}

	function paintPass($message)
	{
	  parent::paintPass($message);
	  $this->paintLine($message, "pass");
	  $this->trId++;
	}

	function paintFail($message)
	{
	  SimpleScorer::paintFail($message);
	  $this->paintLine($message, "fail");
	   $this->trId++;
	}

	protected function getCss()
	{
	  return parent::getCss() . '
	    body { font-size:11px; font-family: tahoma, verdana, arial, helvetica }
	    tr.fail { background-color: red; color:white; font-size:11px;  font-family: tahoma, verdana, arial, helvetica; }
	    tr.pass { color: darkgreen; font-size:11px;  font-family: tahoma, verdana, arial, helvetica;}
	    td.result { font-weight: bold }
	    /* tr.list_0 { background: #FFF; }*/
	    /* tr.list_1 { background:#CCC; } */
	    ';
	}

	function paintHeader($test_name)
	{
	  parent::paintHeader($test_name);
	  echo "<table border=\"1\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">";
	  echo "<thead><tr><th>Data</th><th>Metodo</th><th>Mensagem</th><th>Resultado</th></tr></thead>";
	  echo "<tbody>";
	  flush();
	}

	function paintFooter($test_name)
	{
	  echo "</tbody>";
	  echo "</table>";
	  flush();

	  parent::paintFooter($test_name);
	}
}