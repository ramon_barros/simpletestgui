<!DOCTYPE html>
<html>
<head>
<title>Unit Testing &rsaquo; Index</title>
<link rel="stylesheet" type="text/css" href="simpletest/css/unit_test.css" charset="utf-8">

</head>
<body>
	<div id="header">
		<h2>Unit Testing &rsaquo; Index</h2>
	</div>

	<div id="nav">
		<?php echo form_open($form_url); ?>
			<input type="hidden" name="all" value="1" />
			<input type="submit" value="All" />
		<?php echo form_close(); ?>
		<?php foreach ($loadfiles->get_class() as $class=>$file) { ?>
			<?php echo form_open($form_url); ?>
				<input type="hidden" name="<?php echo strtolower($class);?>" value="1" />
				<input type="submit" value="<?php echo $class;?>" />
			<?php echo form_close(); ?>
		<?php } ?>
	
	<?php
		/*
		if (isset($_POST['test']) && trim($_POST['test']) != "") {
			$testName = explode('/', $_POST['test']);
			$testName = $testName[1];
		}
		else {
			$testName = "";
		}
	?>
		<?php echo form_open($form_url); ?>
			<select name="test">
				<?php foreach ($loadfiles->get_files() as $class=>$files) { ?>
					<optgroup label="<?php echo $class;?>">
						<?php if(is_array($files)) { ?>
								<?php foreach ($files as $file) { ?>
									<?php $filename = explode('/', $file); ?>
									<option value="<?php echo $class."|".end($filename);?>" <?php if ($file == $testName) { echo 'selected="selected"'; } ?>><?php echo end($filename); ?></option>
								<?php } ?>
						<?php } ?>
				</optgroup>
				<?php } ?>
			</select>
			<input type="submit" value="Run" />
		<?php echo form_close();?>
	<?php
		 */ 
	?>
	</div>
		
	<div id="report">
		<?php
		    ob_start();
		    $loadfiles->run_report(new MyReporter());
		    ob_end_flush();
		?>
	</div>
		
</body>
</html>