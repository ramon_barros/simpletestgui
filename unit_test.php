<?php

ini_set('display_errors', 'on');
error_reporting(-1);

/**
 * Retorna o caminho completo do arquivo unit_test.php
 * neste caso /path/test/
 */
define('MAIN_PATH', realpath(dirname(__FILE__)).'/');

/**
 * Define o caminho da pasta do simpletest
 * neste caso /path/test/simpletest/
 */
define('SIMPLETEST', MAIN_PATH.'simpletest/');

/**
 * Define o caminho da pasta onde ficara os testes
 * neste caso /path/test/test/
 */
define('TESTS_DIR', MAIN_PATH.'test/');

/**
 * Define o caminho da pasta das classes
 * neste caso /path/test/classes/
 */
define('CLASS_DIR',MAIN_PATH.'classes/');

$classes = array('Guestbook','web_test.php');
//$classes = 'Guestbook';

require_once SIMPLETEST.'unit_tester.php';
require_once SIMPLETEST.'mock_objects.php';
require_once SIMPLETEST.'collector.php';
require_once SIMPLETEST.'web_tester.php';
require_once SIMPLETEST.'extensions/my_reporter.php';
require_once SIMPLETEST.'extensions/load_files.php';

$loadfiles = new LoadFiles($classes);

function form_open($url){
	return '<form accept-charset="utf-8" method="post" action="'.$url.'">';
}
function form_close(){
	return '</form>';
}

$form_url =  'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

include(SIMPLETEST.'test_gui.php');
